<?php
/**
 * Share class.
 */

namespace Share2017;

/**
 * Main plugin class.
 */
class Share
{

    /**
     * Ideas to generate buttons and metadata for.
     * @var array
     */
    static private $ideas = [
        'eleves'=>[
            'title'=>"Initier à l'engagement et à la responsabilité dès l'école en consultant les élèves",
        ],
        'responsable'=>[
            'title'=>"Favoriser l'engagement à l'université en généralisant le statut de responsable associatif étudiant",
        ],
        'diplomes'=>[
            'title'=>"Créer des diplômes mixtes voire blancs",
        ],
        'service-civique'=>[
            'title'=>"Renforcer les éléments qui distinguent un service civique d'un emploi"
        ],
        'budget'=>[
            'title'=>"Créer un budget participatif étudiant dans chaque établissement",
        ],
        'gouvernance'=>[
            'title'=>"Encourager une gouvernance des campus participative",
        ],
        'environnement'=>[
            'title'=>"Créer une clause d'impact environnemental sur les campus",
        ],
        'connaissances-durable'=>[
            'title'=>"Créer un test de connaissances et de compétences sur le développement durable",
        ],
        'numerique'=>[
            'title'=>"Développer dans les universités des tiers-lieux numériques ouverts sur leur territoire",
        ]
    ];

    /**
     * Get OpenGraph description.
     * @return string
     */
    public function getDesc()
    {
        return "L'ensemble de nos idées pour une société de l'engagement sont sur 2017.animafac.net";
    }

    /**
     * Get OpenGraph title.
     * @return string
     */
    public function getTitle()
    {
        return self::$ideas[$_GET['idea']]['title'];
    }

    /**
     * Get URL to share.
     * @return string
     */
    public function getURL()
    {
        return get_permalink().'?idea='.$_GET['idea'];
    }

    /**
     * Get OpenGraph thumbnail.
     * @return string URL
     */
    public function getImage()
    {
        $url =  plugins_url('/img/idea-'.$_GET['idea'].'.png', __DIR__);
        if (strpos($url, 'www.animafac.net') === false) {
            return 'https://www.animafac.net/'.str_replace('/animafac/', '', $url);
        } else {
            return $url;
        }
    }

    /**
     * Get "2017_share" shortcode result.
     *
     * The shortcode generates a list of share buttons.
     *
     * @return string HTML
     */
    public function getShortcode()
    {
        $html = '
        <div class="widget-bloc-latest">
            <div class="bloc-widget-title">
                <div class="dashicons dashicons-format-status force-red"></div>
                Faites entendre <span class="force-red">vos idées</span>
            </div>
            <div>
                <div id="2017-share-widget" class="panel-group">';
        foreach (self::$ideas as $slug => $idea) {
            $html .= '
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle share-idea-title" data-toggle="collapse"
                        data-parent="#2017-share-widget" href="#2017-share-widget-'.$slug.'">
                            <span class="dashicons dashicons-editor-quote force-red"> </span>'.$idea['title'].'
                        </a>
                    </h4>
                </div>
                <div id="2017-share-widget-'.$slug.'" class="panel-collapse collapse" style="height: auto;">
                    <div class="panel-body">
                        <div class="widget-bloc-all">
                            <p><a target="_blank" href="https://www.animafac.net/minisite/engageons-2017/synthese-des-propositions/#idea-'.$slug.'">
                                Découvrir cette proposition</a></p>
                            <br/>
                            <p style="text-align: center;"><b>Je la partage sur&nbsp;:</b></p>
                            <ul class="rrssb-buttons clearfix" style="width: 84px; margin: auto;">
                                <li class="rrssb-facebook">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u='.urlencode('https://www.animafac.net/minisite/engageons-2017/synthese-des-propositions/?idea='.$slug.'#idea-'.$slug).'" class="popup">
                                        <span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 29"><path d="M26.4 0H2.6C1.714 0 0 1.715 0 2.6v23.8c0 .884 1.715 2.6 2.6 2.6h12.393V17.988h-3.996v-3.98h3.997v-3.062c0-3.746 2.835-5.97 6.177-5.97 1.6 0 2.444.173 2.845.226v3.792H21.18c-1.817 0-2.156.9-2.156 2.168v2.847h5.045l-.66 3.978h-4.386V29H26.4c.884 0 2.6-1.716 2.6-2.6V2.6c0-.885-1.716-2.6-2.6-2.6z"/></svg></span>
                                        <span class="rrssb-text">Facebook</span>
                                    </a>
                                </li>
                                <li class="rrssb-twitter">
                                    <a href="https://twitter.com/intent/tweet?text='.urlencode($idea['title'].' https://www.animafac.net/minisite/engageons-2017/synthese-des-propositions/?idea='.$slug.'#idea-'.$slug).'&amp;via=animafac" class="popup">
                                        <span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28"><path d="M24.253 8.756C24.69 17.08 18.297 24.182 9.97 24.62a15.093 15.093 0 0 1-8.86-2.32c2.702.18 5.375-.648 7.507-2.32a5.417 5.417 0 0 1-4.49-3.64c.802.13 1.62.077 2.4-.154a5.416 5.416 0 0 1-4.412-5.11 5.43 5.43 0 0 0 2.168.387A5.416 5.416 0 0 1 2.89 4.498a15.09 15.09 0 0 0 10.913 5.573 5.185 5.185 0 0 1 3.434-6.48 5.18 5.18 0 0 1 5.546 1.682 9.076 9.076 0 0 0 3.33-1.317 5.038 5.038 0 0 1-2.4 2.942 9.068 9.068 0 0 0 3.02-.85 5.05 5.05 0 0 1-2.48 2.71z"/></svg></span>
                                        <span class="rrssb-text">Twitter</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                     </div>
                 </div>
            </div>';
        }
        $html .= '</div></div></div>';
        return $html;
    }

    /**
     * Add a custom item to the TOC.
     * @return string HTML
     */
    public function getTOC()
    {
        $minisites = wp_get_object_terms(get_the_ID(), 'minisite');
        if ($minisites[0]->slug == 'engageons-2017') {
            echo '<a href="https://2017.animafac.net/Engageons-2017/">
                <div class="widget-news-bloc row">
                    <div class="news-bloc-title col-md-12">Vos témoignages</div>
                </div>
            </a>';
        }
    }

    /**
     * Replace thumbnails used by our SEO plugin.
     * @param  array $tags
     * @return array
     */
    public function replaceImages($tags)
    {
        //Remove original images
        unset($tags['http://ogp.me/ns#image']);
        $tags['http://ogp.me/ns#image'] = self::getImage();
        return $tags;
    }
}
