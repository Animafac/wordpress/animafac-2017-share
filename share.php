<?php
/*
 * Plugin Name: Partage d'idées d'Engageons 2017
 * Plugin URI: https://framagit.org/Animafac/animafac-2017-share
 * Description: Module de partage sur les réseaux sociaux pour la campagne Engageons 2017
 * Version: 0.1.0
 */

require_once __DIR__.'/vendor/autoload.php';

use Share2017\Share;

if (isset($_GET['idea']) && !empty($_GET['idea'])) {
    add_filter('wpseo_opengraph_desc', [Share::class, 'getDesc']);
    add_filter('wpseo_twitter_description', [Share::class, 'getDesc']);

    add_filter('wpseo_opengraph_title', [Share::class, 'getTitle']);
    add_filter('wpseo_twitter_title', [Share::class, 'getTitle']);

    add_filter('wpseo_canonical', '__return_false');
    add_filter('facebook_rel_canonical', [Share::class, 'getURL']);

    add_filter('wpseo_opengraph_image', [Share::class, 'getImage']);
    add_filter('wpseo_twitter_image', [Share::class, 'getImage']);
    add_filter('fb_meta_tags', [Share::class, 'replaceImages']);
}

add_shortcode('2017_share', [Share::class, 'getShortcode']);
add_action('minisite-toc', [Share::class, 'getTOC']);
