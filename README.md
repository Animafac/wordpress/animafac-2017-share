# animafac-2017-share

This WordPress plugin adds Facebook and Twitter metadata used in the [*Engageons 2017*](https://www.animafac.net/minisite/engageons-2017/synthese-des-propositions/) campaign by [Animafac](https://www.animafac.net/).
It is specific to this website and will not work correctly on another WordPress.

## How it works

When the GET parameter `idea` is detected, it will adjust the page metadata to match the specified idea.
This way, one page can have different metadata depending on the share button used by the user.

The plugin also adds a `2017_share` shortcode that generates the various share buttons.

## Credits

Only the code is available under a GPL licence.
Images are owned by [Cyrielle Evrard](http://www.cy-clone.fr/).
